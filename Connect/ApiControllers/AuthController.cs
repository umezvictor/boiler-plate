﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Connect.Core.Services;
using Connect.Core.Utils;
using Connect.Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Connect.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IUserManagementService userMgtService;
        private readonly IEmailService emailService;

        public AuthController(IUserManagementService userMgtService, IEmailService emailService)
        {
            this.userMgtService = userMgtService;
            this.emailService = emailService;
        }
       
        [HttpPost("signup")]      
        public async Task<IActionResult> Signup([FromBody] AppUserVM appUserVM)
        {
            try
            {
                if (appUserVM.email.Length == 0 || appUserVM.password.Length == 0 || appUserVM.LastName.Length == 0 || 
                    appUserVM.FirstName.Length == 0)
                {
                    return BadRequest("Please fill all required fields");
                }

                var result = await userMgtService.AddUser(appUserVM);
                if (result == (true, "CREATED", null))
                {
                    return Ok("User created");
                }

                else if(result == (false, "", null))
                {
                    return BadRequest("User already exists");
                }
                else
                {
                    List<IEnumerable<IdentityError>> response = new List<IEnumerable<IdentityError>>();

                    response.Add(result.Item3);
                    foreach(var item in response[0])
                    {
                        if(item.Description.Length > 0)
                        {
                            return BadRequest(item.Description);
                        }
                    }
                }
                
                return null;
               
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { message = "Something went wrong"});
            }
        }


        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginVM loginVM)
        {
            try
            {
                if (loginVM.Email.Length == 0 || loginVM.Password.Length == 0 )
                {
                    return BadRequest("Password and email are required fields");

                }

                var result = await userMgtService.LoginUser(loginVM);
                //The result is a token string
                if(result.Length == 0)
                {
                    return BadRequest("Incorrect username or password");
                }

                return Ok(result);

            }
            catch(Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { message = "Something went wrong"});
            }
        }

        [HttpPost("verifyEmail")]
        public async Task<IActionResult> VerifyEmail([FromQuery] string token, [FromQuery] string email)
        {
            try
            {
                var response = await userMgtService.VerifyEmail(token, email);
                if (response == "NOTFOUND")
                {
                    return NotFound("User not found");
                }

                if (response == "SUCCESS")
                {
                    return Ok("Email verified successfully");
                }

                if (response == "ERROR")
                {
                    return BadRequest("Email verification failed");
                }

                return null;


            }
            catch(Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { messsage = ex.Message });
            }
        }

        //sends password reset link to user
        [HttpPost("forgotPassword")]
        public async Task<IActionResult> SendPasswordResetLink(ForgotPasswordVM vm)
        {
            try
            {
                if (vm.Email.Length == 0) return BadRequest("Email is resquired");

                var response = await userMgtService.ForgotPassword(vm);
                if (response == "NOTFOUND")
                {
                    return NotFound("User not found");
                }

                if (response == "SUCCESS")
                {
                    return Ok("Password reset link sent");
                }

                return BadRequest("Password reset link not sent");

            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { messsage = ex.Message });
            }
        }

        //when user clicks on password reset link from email, he's routed to this endpoint
        //captures token and email from url query
        [HttpGet("resetPassword")]
        public IActionResult ResetPassword([FromQuery] string token, [FromQuery] string email)
        {
            var model = new ResetPasswordVM { Token = token, Email = email };
            return Ok(model);
        }

        [HttpPost("resetPassword")]
        public async Task<IActionResult> ResetPassword(ResetPasswordVM vm)
        {
            try
            {
                if (vm.Token.Length == 0 && vm.Email.Length == 0 && vm.Password.Length == 0)
                {
                    return BadRequest("Token, email and new password are required");
                }

                var response = await userMgtService.ResetPassword(vm);

                if (response == "SUCCESS")
                {
                    return Ok("Password reset successful");
                }
                if (response == "NOTFOUND")
                {
                    return NotFound("User not found");
                }

                return null;
            }
            catch(Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { messsage = ex.Message });
            }
        }

    }
}
