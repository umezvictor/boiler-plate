﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Connect.Core.Services;
using Connect.Core.Utils;
using Connect.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Connect.ApiControllers
{
    public class UserController : ControllerBase
    {

        private readonly IUserManagementService userMgtService;
        private readonly IEmailService emailService;

        public UserController(IUserManagementService userMgtService)
        {
            this.userMgtService = userMgtService;
        }


        //get roles user belongs to
        [HttpGet("/roles/{id}")]
        public async Task<IActionResult> GetUserRoles(string id)
        {
            try
            {
                var roles = await userMgtService.GetSelectedAndNonSelectedRoles(id);
                if (roles == null)
                {
                    return NotFound("No record found");
                }

                return Ok(roles);
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message.ToString());
                return this.StatusCode(500, new { message = "Something went wrong" });
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var users = await userMgtService.GetUsers();
                if (users == null)
                {
                    return NotFound("No record found");
                }

                return Ok(users);
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message.ToString());
                return this.StatusCode(500, new { message = "Something went wrong" });
            }
        }

        //assign role(s) to a user
        [HttpPost("/roles/{id}")]
        public async Task<IActionResult> AddUserToRole([FromBody] List<AppRoleVM> model, string id)
        {
            try
            {
                var response = await userMgtService.AssignRoleToUser(model, id);

                if (response == "OK")
                {
                    return Ok();
                }

                return BadRequest("Please make a selection");
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message.ToString());
                return this.StatusCode(500, new { message = "Something went wrong" });
            }
        }
    }
}
