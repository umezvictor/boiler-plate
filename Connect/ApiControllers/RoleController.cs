﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Connect.Core.Services;
using Connect.Core.Utils;
using Connect.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Connect.ApiControllers
{
    public class RoleController : ControllerBase
    {

        private readonly IUserManagementService userMgtService;
        private readonly IEmailService emailService;

        public RoleController(IUserManagementService userMgtService)
        {
            this.userMgtService = userMgtService;
        }

       
        [HttpPost]
        public async Task<IActionResult> AddRole(AppRoleVM vm)
        {
            try
            {
                if (vm.Name.Length == 0) return BadRequest("Role name is required");

                var response = await userMgtService.AddRole(vm);

                if (response == "CREATED")
                {
                    return Ok("Role created");
                }

                if (response == "ERROR")
                {
                    return BadRequest("Role creation failed");
                }

                if (response == "EXISTS")
                {
                    return BadRequest("Role already exists");
                }

                return null;
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { messsage = ex.Message });

            }

        }

        [HttpGet]
        public async Task<IActionResult> GetRoles()
        {
            try
            {
                var response = await userMgtService.GetRoles();
                if (response == null)
                {
                    return NotFound("No roles creatd yet");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { messsage = ex.Message });
            }

        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetRoleById(string id)
        {
            try
            {
                var response = await userMgtService.GetRoleById(id);
                if (response == null)
                {
                    return NotFound("NOTFOUND");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { messsage = ex.Message });
            }

        }

        [HttpPut]
        public async Task<IActionResult> EditRole(AppRoleVM vm)
        {
            try
            {
                var response = await userMgtService.EditRole(vm);

                if (response == "SUCCESS")
                {
                    return Ok("Role updated successfully");
                }

                if (response == "NOTFOUND")
                {
                    return NotFound("Role not found");
                }

                return BadRequest("Role update failed");
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { messsage = ex.Message });
            }

        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRole(string id)
        {
            try
            {
                var response = await userMgtService.DeleteRole(id);
                if (response == "SUCCESS")
                {
                    return Ok("Role deleted");
                }
                return BadRequest("Role deletion failed");
            }
            catch (Exception ex)
            {
                Logger.writeErrorLog(ex.Message);
                return this.StatusCode(500, new { messsage = ex.Message });
            }

        }
    }
}
