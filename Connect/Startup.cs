using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DbUp;
using Connect.Core.EF;
using Connect.Core.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Connect.Security;
using Connect.Core.Models;


namespace Connect
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment hostEnvironment)
        {
            Configuration = configuration;
            HostEnvironment = hostEnvironment;
        }

        public IConfiguration Configuration { get; }
        public static IWebHostEnvironment HostEnvironment { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {


            services.Configure<IdentityOptions>(options =>
            {
                //Password Settings
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 4;
                options.Password.RequiredUniqueChars = 1;
                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
                //  options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

            });

            //enable caching

            //in memory caching
            services.AddMemoryCache();

            //redis caching
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost:6379";
                options.InstanceName = "redisConnection";
                
            });

            AddCustomDbContext(services);
            AddCustomIdentity(services);
            ConfigureDIService(services);
            services.AddControllers();


            services.AddLazyCache();
            services.AddMvc(options =>
            {
                // options.Filters.Add(typeof(VerveAccessException));
            });


            services.AddAuthorization(options =>
            {
                options.AddPolicy(AdminClaims.Add, policy => { policy.RequireClaim(CustomClaimTypes.Permission, AdminClaims.Add); });
                options.AddPolicy(AdminClaims.Edit, policy => { policy.RequireClaim(CustomClaimTypes.Permission, AdminClaims.Edit); });
                options.AddPolicy(AdminClaims.Edit, policy => { policy.RequireClaim(CustomClaimTypes.Permission, AdminClaims.Delete); });
                options.AddPolicy(UserClaims.View, policy => { policy.RequireClaim(CustomClaimTypes.Permission, UserClaims.View); });
            });

           

            services.AddRazorPages().AddRazorRuntimeCompilation();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();

        }

        public IServiceCollection AddCustomDbContext(IServiceCollection services)
        {
            string dbConnStr = Configuration.GetConnectionString("Default");//specifies the connection string to use 'Default' in this case
            services.AddDbContext<ConnectDbContext>(options =>
            {
                options.UseSqlServer(dbConnStr,
                     b => b.MigrationsAssembly("Connect.Core"));//specifies which assembly the migration files are
            });

            return services;
        }



        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, UserManager<ConnectUser> userManager, 
            RoleManager<ConnectRole> roleManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Admin}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

        }
    }
}

