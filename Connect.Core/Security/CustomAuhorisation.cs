﻿using Connect.Core.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Connect.Security
{
    internal class CustomClaimTypes
    {
        public const string Permission = "Application.Permission";
    }

    internal static class UserClaims
    {
        public const string View = "users.view";
      
    }

    internal static class AdminClaims
    {
        public const string Add = "users.add";
        public const string Edit = "users.edit";
        public const string Delete = "users.delete";
    }




    public static class UserAndRoleDataInitializer
    {
        public static void SeedData(UserManager<ConnectUser> userManager, RoleManager<ConnectRole> roleManager)
        {
           SeedRoles(roleManager);
           SeedUsers(userManager);
           SeedUserClaims(roleManager);

        }

        private static void SeedUsers(UserManager<ConnectUser> userManager)
        {
            string email = "dennis@gmail.com";
            string password = "123456";
             string  username = "user";

            string email2 = "dennis@gmail2.com";
            string username2 = "admin";

            if (userManager.FindByEmailAsync(email).Result == null)
            {
                ConnectUser user = new ConnectUser();
                user.UserName = username;
                user.Email = email;
                user.FirstName = "Dennis";
                user.LastName = "Osas";

                IdentityResult result = userManager.CreateAsync(user, password).Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "User").Wait();
                }
            }

            if (userManager.FindByEmailAsync(email2).Result == null)
            {
                ConnectUser user = new ConnectUser();
                user.UserName = username2;
                user.Email = email2;
                user.FirstName = "Dennis";
                user.LastName = "Osas";

                IdentityResult result = userManager.CreateAsync(user, password).Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }

        }

        private static void SeedRoles(RoleManager<ConnectRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("User").Result)
            {
                ConnectRole role = new ConnectRole();
                role.Name = "User";
                var roleResult = roleManager.CreateAsync(role).Result;
            }


            if (!roleManager.RoleExistsAsync("Admin").Result)
            {
                ConnectRole role = new ConnectRole();
                role.Name = "Admin";
                var roleResult = roleManager.CreateAsync(role).Result;
            }
        }
   
        private static void SeedUserClaims(RoleManager<ConnectRole> roleManager)
        {

            var userRole = roleManager.FindByNameAsync("User").Result;
            var adminRole = roleManager.FindByNameAsync("Admin").Result;

            roleManager.AddClaimAsync(userRole, new Claim(CustomClaimTypes.Permission, UserClaims.View));
             roleManager.AddClaimAsync(adminRole, new Claim(CustomClaimTypes.Permission, AdminClaims.Add));
            roleManager.AddClaimAsync(adminRole, new Claim(CustomClaimTypes.Permission, AdminClaims.Edit));
            roleManager.AddClaimAsync(adminRole, new Claim(CustomClaimTypes.Permission, AdminClaims.Delete));

           
        }
    
    }

}
