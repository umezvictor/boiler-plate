﻿using Connect.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Connect.Core.ViewModels
{
  public  class AppRoleVM
    {
       
        public string Id { get; set; }
       
        public string  Name { get; set; }
        public bool  IsSelected { get; set; }

       
       // public List<string> Claims { get; set; }
        public string oldRole { get; set; }

        public static explicit operator AppRoleVM(ConnectRole source)
        {
            var destination = new AppRoleVM()
            {
                Id = source.Id.ToString(),
                Name = source.Name,
                IsSelected = source.IsSelected
                
            };
            return destination;
        }
    }
}
