﻿using Connect.Core.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Connect.Core.ViewModels
{
   public class AppUserVM
    {
        public string Id { get; set; } 
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string email { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string phoneNumber { get; set; }
    
        public string Gender { get; set; }

        public string RoleName { get; set; }

        public static explicit operator AppUserVM(ConnectUser source)
        {
            var destination = new AppUserVM()
            {
                    Id = source.Id.ToString(),
                    LastName = source.LastName,
                    FirstName = source.FirstName,
                    email =source.Email,
                    userName  = source.Email,
                    phoneNumber = source.PhoneNumber
            };
            return destination;
        }
    }
}
