﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Connect.Core.Models.Map
{
    public class UserRoleMap : IEntityTypeConfiguration<ConnectUserRole>
    {
        public void Configure(EntityTypeBuilder<ConnectUserRole> builder)
        {
            builder.HasKey(p => new { p.UserId, p.RoleId });
        
        }

       
    }
}