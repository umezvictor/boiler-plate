﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Connect.Core.Models.Map
{
    public class RoleMap : IEntityTypeConfiguration<ConnectRole>
    {
        public void Configure(EntityTypeBuilder<ConnectRole> builder)
        {
            SetupData(builder);
        }

        private void SetupData(EntityTypeBuilder<ConnectRole> builder)
        {
           var roles = new ConnectRole[]
           {
               new ConnectRole
               {
                   Id =Guid.NewGuid(),
                   Name = "SYS_ADMIN",
                   NormalizedName = "SYS_ADMIN"
               }
           };

           builder.HasData(roles);
        }
    }
}