﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Connect.Core.Models.Map
{
    public class UserTokenMap: IEntityTypeConfiguration<ConnectUserToken>
    {
        public void Configure(EntityTypeBuilder<ConnectUserToken> builder)
        {
            builder.HasKey(p => p.UserId);
        }
    }
}
