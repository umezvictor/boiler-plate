using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;

namespace Connect.Core.Models.Map
{
    public class UserMap : IEntityTypeConfiguration<ConnectUser>
    {
        public UserMap()
        {
        }
        public PasswordHasher<ConnectUser> Hasher { get; set; } = new PasswordHasher<ConnectUser>();

        public void Configure(EntityTypeBuilder<ConnectUser> builder)
        {
            builder.Property(b => b.FirstName).HasMaxLength(150);
            builder.Property(b => b.LastName).HasMaxLength(150);
           
           
        }

      
    }
}