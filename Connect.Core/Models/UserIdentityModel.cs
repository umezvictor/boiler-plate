﻿using Connect.Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Connect.Core.Models
{
    [Table(nameof(ConnectUser))]
    public class ConnectUser :  IdentityUser<Guid>
    {
        public ConnectUser()
        {
            Id = Guid.NewGuid();
            CreatedOnUtc = DateTime.UtcNow;
        }

        public string RefreshToken { get; set; }
        public string ProviderKey { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }

        



        [NotMapped]
        public string FullName
        {
            get
            {

                return $"{LastName} {FirstName}";
            }
        }

        public DateTime CreatedOnUtc { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public bool Activated { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime ModifiedOnUtc { get; set; }
    }

    public class ConnectUserClaim : IdentityUserClaim<Guid>
    {
    }

    public class ConnectUserLogin : IdentityUserLogin<Guid>
    {
        [Key]
        [Required]
        public int Id { get; set; }
    }

    public class ConnectRole : IdentityRole<Guid>
    {
        public ConnectRole()
        {
            Id = Guid.NewGuid();
            ConcurrencyStamp = Guid.NewGuid().ToString("N");
        }

        public bool IsSelected { get; set; }
    }


    public class ConnectUserRole : IdentityUserRole<Guid>
    {
        //[Key]
        //[Required]
        //public int Id { get; set; }
    }

    public class ConnectRoleClaim : IdentityRoleClaim<Guid>
    {
       
    }

    public class ConnectUserToken : IdentityUserToken<Guid>
    {
        [Key]
        [Required]
        public int Id { get; set; }
    }
}
