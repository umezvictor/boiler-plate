﻿using Connect.Core.Models;
using Connect.Core.Utils;
using Connect.Core.ViewModels;
using Connect.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Connect.Core.Services
{
    public class UserManagementService : IUserManagementService
    {

        private readonly UserManager<ConnectUser> userManager;
        private readonly RoleManager<ConnectRole> roleManager;
        private readonly SignInManager<ConnectUser> signInManager;
        private readonly IConfiguration configuration;
 

        public UserManagementService(UserManager<ConnectUser> userManager, RoleManager<ConnectRole> roleManager,
            SignInManager<ConnectUser> signInManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
        }


        public async Task<(bool, string, IEnumerable<IdentityError>)> AddUser(AppUserVM userVM)
        {
            //url is the base url for the site
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("AppConfig");
            string url = config["baseurl"];

            if (await userManager.FindByEmailAsync(userVM.email) != null)
            {
                return (false, "", null);
            }

            var user = new ConnectUser
            {
                Email = userVM.email,
                FirstName = userVM.FirstName,
                LastName = userVM.LastName,
                UserName = userVM.email,
                PhoneNumber = userVM.phoneNumber
            };

            var result = await userManager.CreateAsync(user, userVM.password);

            if (result.Succeeded)
            {              
                //send verification email to user
                var token = await userManager.GenerateEmailConfirmationTokenAsync(user);
                //encode token to base 64
                byte[] tokenGeneratedBytes = Encoding.UTF8.GetBytes(token);
                var encodedToken = WebEncoders.Base64UrlEncode(tokenGeneratedBytes);
                var confirmationLink = $"{url}/api/auth/verifyEmail?token={encodedToken}&email={user.Email}";

                EmailService emailService = new EmailService();
                //send email verification link to user
                await emailService.SendEmail(user.Email, "Email Verification Link", confirmationLink);
                return (true, "CREATED", null);
            }
            //falls here if username or password doesn't meet requirements, eg too short
            return (false, "", result.Errors);
        }

  
        public async Task<IEnumerable<ConnectUser>> GetUsers()
        {
            var users = await userManager.Users.ToListAsync();

            return users;
        }

      
        public async Task<ConnectUser> GetUserById(string userId)
        {
            var user = await userManager.FindByIdAsync(userId);

            return user;
        }

        public async Task<string> LoginUser(LoginVM loginVM)
        {
            var result = await signInManager.PasswordSignInAsync(loginVM.Email, loginVM.Password, loginVM.RememberMe, false);
            if (result.Succeeded)
            {
                var user = await userManager.FindByEmailAsync(loginVM.Email);
                //generate jwt token (pass user claims)
                TokenService tokenService = new TokenService(configuration);

                //create claims
                var claims = new List<Claim>();
                claims.Add(new Claim("Id", user.Id.ToString()));
                claims.Add(new Claim("Email", user.Email));
                claims.Add(new Claim("FirstName", user.FirstName));
                claims.Add(new Claim("LastName", user.LastName));

                var token = tokenService.GenerateAccessToken(claims);
                return token;
            }

            return null;
        }

        public async Task<string> VerifyUser(string Id)
        {
            var user = await userManager.FindByIdAsync(Id);
            if (user.Id == Guid.Parse(Id))
            {
                return "OK";
            }

            return "NOTFOUND";
        }

        public async Task<string> VerifyEmail(string token, string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return "NOTFOUND";
            }

            //decode token 
            var decodedToken = WebEncoders.Base64UrlDecode(token);
            var encodeToken = Encoding.UTF8.GetString(decodedToken);

            var result = await userManager.ConfirmEmailAsync(user, encodeToken);
            if (result.Succeeded)
            {
                return "SUCCESS";
            }

            return "ERROR";
        }

        //sends password reset link to user
        public async Task<string> ForgotPassword(ForgotPasswordVM forgotPasswordVM)
        {

            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("AppConfig");
            string url = config["baseurl"];

            EmailService emailService = new EmailService();
            //check if user exists
            var user = await userManager.FindByEmailAsync(forgotPasswordVM.Email);
            if (user == null)
                //redirect user to forgot password confirmation page -- to avoid account enumeration and brute force attacks
                return "NOTFOUND";
            //generate password reset token
            var token = await userManager.GeneratePasswordResetTokenAsync(user);
            var passwordResetLink = $"{url}/api/auth/resetPassword?token={token}&email={user.Email}";
            //Send password reset link via email
            await emailService.SendEmail(user.Email, "Password Reset Link", passwordResetLink);
            return "SUCCESS";
        }

        //handles post request for resetPassword 
        public async Task<string> ResetPassword(ResetPasswordVM resetPasswordVM)
        {
           //check if user exists
            var user = await userManager.FindByEmailAsync(resetPasswordVM.Email);
            if (user == null)
            {
                return "NOTFOUND";
            }
            //token and email are captured in the get request and saved in hidden fields before submitting post request
            //reset password
            var resetPassResult = await userManager.ResetPasswordAsync(user, resetPasswordVM.Token,
                resetPasswordVM.Password);

            if (resetPassResult.Succeeded)
            {
                return "SUCCESS";
            }
            return "ERROR";
        }

        public async Task<string> AddRole(AppRoleVM vm)
        {            
            //check if role already exists
            if (!roleManager.RoleExistsAsync(vm.Name).Result)
            {
                //add the new role
                ConnectRole role = new ConnectRole();
                role.Name = vm.Name;
                var response = await roleManager.CreateAsync(role);
                if (response.Succeeded)
                {
                    return "CREATED";
                }
                return "ERROR";
            }

            return "EXISTS";
        }


        //get all roles, but indicate which ones user belongs to
        //with this, on the ui, you can indicate which role the user already belongs by checking the checkbox
        public async Task<IEnumerable<AppRoleVM>> GetSelectedAndNonSelectedRoles(string userId)
        {
            var user = await userManager.FindByIdAsync(userId);
            var roles = await roleManager.Roles.ToListAsync();
            if (roles == null)
            {
                return null;
            }

            AppRoleVM model = new AppRoleVM();
            List<AppRoleVM> rolesToDisplay = new List<AppRoleVM>();

            foreach (var role in roles)
            {
                //check if user belongs to role
                if (await userManager.IsInRoleAsync(user, role.ToString()))
                {

                    model.IsSelected = true;
                }
                else
                {
                    model.IsSelected = false;
                }

                model.Name = role.Name;

                rolesToDisplay.Add(model);
            }

            return rolesToDisplay;
        }

        //assign role/roles to a user
        public async Task<string> AssignRoleToUser(List<AppRoleVM> roleVM, string userId)
        {
            //get user
            var user = await userManager.FindByIdAsync(userId);
           
            //check for selected roles
            for (int i = 0; i < roleVM.Count; i++)
            {
                //check which one was selected by user
                if (roleVM[i].IsSelected)
                {
                    //check if user has already been added to this role before
                    if (await userManager.IsInRoleAsync(user, roleVM[i].Name))
                    {
                        //skip and check the next role
                        continue;
                    }
                    //if user selects this role, and has not been added to this role before, add him to the role
                    var result = await userManager.AddToRoleAsync(user, roleVM[i].Name);
                    if (result.Succeeded)
                    {
                        //check if we have gotten to end of the loop
                        if (i == (roleVM.Count - 1)) return "OK";

                        //Check if we still have some items to loop through
                        if (i < (roleVM.Count - 1)) continue;
                    }
                }
                else
                {
                    //if not selected, check if it user belongs to the role however
                    //if he does, means the user has unchecked it, hence we remove him from that role
                    var result = await userManager.RemoveFromRoleAsync(user, roleVM[i].Name);
                    if (result.Succeeded)
                    {
                        //check if we have gotten to end of the loop
                        if (i == (roleVM.Count - 1)) return "OK";

                        //Check if we still have some items to loop through
                        if (i < (roleVM.Count - 1)) continue;
                    }                  
                }
            }
            return null;
        }

        public async Task<IEnumerable<ConnectRole>> GetRoles()
        {
            var roles = await roleManager.Roles.ToListAsync();
            if (roles == null)
            {
                return null;
            }
            return roles;
        }

        //handles the get request
        //renders the role on the edit page
        public async Task<AppRoleVM> GetRoleById(string Id)
        {

            var existingRole = await roleManager.FindByIdAsync(Id);
            if (existingRole != null)
            {
                AppRoleVM appRoleVM = new AppRoleVM();
                appRoleVM.Name = existingRole.Name;
                appRoleVM.Id = existingRole.Id.ToString();
                appRoleVM.oldRole = existingRole.Name;
                return appRoleVM;
            }
            return null;
        }
        //handles post request
        public async Task<string> EditRole(AppRoleVM vm)
        {
            if (vm.oldRole != vm.Name)
            {
                var existingRole = roleManager.Roles.FirstOrDefault(x => x.Name == vm.oldRole);
                if (existingRole != null)
                {
                    existingRole.Name = vm.Name;
                    var result = await roleManager.UpdateAsync(existingRole);
                    if (result.Succeeded)
                    {
                        return "SUCCESS";
                    }
                }
                return "NOTFOUND";
            }
            return "ERROR";

        }
        //chaps --
        public async Task<string> DeleteRole(string id)
        {
            var role = await roleManager.FindByIdAsync(id);

            var response = await roleManager.DeleteAsync(role);
            if (response.Succeeded)
            {
                return "SUCCESS";
            }

            return "ERROR";
        }
    }
}
