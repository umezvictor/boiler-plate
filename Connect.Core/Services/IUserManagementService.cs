﻿using Connect.Core.Models;
using Connect.Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Connect.Core.Services
{
    public interface IUserManagementService
    {
        Task<string> AddRole(AppRoleVM vm);
        Task<(bool, string, IEnumerable<IdentityError>)> AddUser(AppUserVM userVM);
        Task<string> AssignRoleToUser(List<AppRoleVM> roleVM, string userId);
        Task<string> DeleteRole(string id);
        Task<string> EditRole(AppRoleVM vm);
        Task<string> ForgotPassword(ForgotPasswordVM forgotPasswordVM);
        Task<AppRoleVM> GetRoleById(string Id);
        Task<IEnumerable<ConnectRole>> GetRoles();
        Task<IEnumerable<AppRoleVM>> GetSelectedAndNonSelectedRoles(string userId);
        Task<ConnectUser> GetUserById(string userId);
        Task<IEnumerable<ConnectUser>> GetUsers();
        Task<string> LoginUser(LoginVM loginVM);
        Task<string> ResetPassword(ResetPasswordVM resetPasswordVM);
        Task<string> VerifyEmail(string token, string email);
        Task<string> VerifyUser(string Id);
    }
}