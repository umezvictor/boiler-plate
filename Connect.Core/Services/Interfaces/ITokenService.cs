﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Connect.Core.Services
{
    public interface ITokenService
    {
        string GenerateAccessToken(IEnumerable<Claim> claims);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
        bool ValidateToken(string token, out string username);
    }
}